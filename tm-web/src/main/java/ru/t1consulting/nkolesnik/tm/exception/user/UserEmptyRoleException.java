package ru.t1consulting.nkolesnik.tm.exception.user;

public final class UserEmptyRoleException extends AbstractUserException {

    public UserEmptyRoleException() {
        super("Error! User role is empty...");
    }

}