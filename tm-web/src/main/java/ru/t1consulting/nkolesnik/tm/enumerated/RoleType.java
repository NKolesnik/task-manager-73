package ru.t1consulting.nkolesnik.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum RoleType {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    RoleType(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static RoleType toRole(@Nullable final String value) {
        if (value == null || value.isEmpty())
            return USUAL;
        for (RoleType role : values()) {
            if (role.name().equals(value))
                return role;
        }
        return USUAL;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
