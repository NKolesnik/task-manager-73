package ru.t1consulting.nkolesnik.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.model.ICommand;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class HelpListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String DESCRIPTION = "Show terminal command list.";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@helpListener.getName() == #event.name || @helpListener.getArgument() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (ICommand command : commands) {
            System.out.println(command);
        }
    }

}
