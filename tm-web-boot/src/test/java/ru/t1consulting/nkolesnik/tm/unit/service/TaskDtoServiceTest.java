package ru.t1consulting.nkolesnik.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.task.TaskEmptyIdException;
import ru.t1consulting.nkolesnik.tm.exception.task.TaskNullException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyIdException;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskDtoServiceTest {

    @NotNull
    private static String userId;
    @NotNull
    @Autowired
    private ITaskDtoService taskService;
    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;
    @Nullable
    private TaskDto task;

    @Before
    public void setUp() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        taskService.clear(userId);
        task = taskService.create(userId);
    }

    @After
    public void tearDown() throws Exception {
        taskService.clear(userId);
    }

    @Test
    public void create() {
        taskService.clear(userId);
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.create(null));
        @NotNull final TaskDto taskDto = taskService.create(userId);
        Assert.assertNotNull(taskDto);
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.existsById(null, task.getId()));
        Assert.assertThrows(TaskEmptyIdException.class, () -> taskService.existsById(userId, null));
        boolean exists = taskService.existsById(userId, task.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.findById(null, task.getId()));
        Assert.assertThrows(TaskEmptyIdException.class, () -> taskService.findById(userId, null));
        @Nullable final TaskDto taskDto = taskService.findById(userId, task.getId());
        Assert.assertNotNull(taskDto);
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.findAll(null));
        @Nullable final List<TaskDto> tasks = taskService.findAll(userId).stream().collect(Collectors.toList());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void count() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.count(null));
        long count = taskService.count(userId);
        Assert.assertEquals(1L, count);
    }

    @Test
    public void save() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.save(null, task));
        Assert.assertThrows(TaskNullException.class, () -> taskService.save(userId, null));
        @Nullable TaskDto taskDto = taskService.findById(userId, task.getId());
        Assert.assertNotNull(taskDto);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertNotEquals(newStatus, taskDto.getStatus());
        taskDto.setStatus(newStatus);
        taskService.save(userId, taskDto);
        taskDto = taskService.findById(userId, task.getId());
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskDto.getStatus(), newStatus);
    }

    @Test
    public void deleteById() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.deleteById(null, task.getId()));
        Assert.assertThrows(TaskEmptyIdException.class, () -> taskService.deleteById(userId, null));
        @Nullable TaskDto taskDto = taskService.findById(userId, task.getId());
        Assert.assertNotNull(taskDto);
        taskService.deleteById(userId, taskDto.getId());
        taskDto = taskService.findById(userId, taskDto.getId());
        Assert.assertNull(taskDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.delete(null, task));
        Assert.assertThrows(TaskNullException.class, () -> taskService.delete(userId, null));
        @Nullable TaskDto taskDto = taskService.findById(userId, task.getId());
        Assert.assertNotNull(taskDto);
        taskService.delete(userId, task);
        taskDto = taskService.findById(userId, taskDto.getId());
        Assert.assertNull(taskDto);
    }

    @Test
    public void deleteAll() {
        List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task);
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.deleteAll(null, tasks));
        Assert.assertThrows(TaskNullException.class, () -> taskService.deleteAll(userId, null));
        @Nullable TaskDto taskDto = taskService.findById(userId, task.getId());
        Assert.assertNotNull(taskDto);
        taskService.deleteAll(userId, tasks);
        taskDto = taskService.findById(userId, taskDto.getId());
        Assert.assertNull(taskDto);
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserEmptyIdException.class, () -> taskService.clear(null));
        long count = taskService.count(userId);
        Assert.assertEquals(1L, count);
        taskService.clear(userId);
        count = taskService.count(userId);
        Assert.assertEquals(0L, count);
    }

}