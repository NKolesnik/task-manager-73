package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.service.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.exception.user.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.model.dto.CustomUser;
import ru.t1consulting.nkolesnik.tm.model.dto.RoleDto;
import ru.t1consulting.nkolesnik.tm.model.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserDtoService service;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String login) throws UsernameNotFoundException {
        @Nullable final UserDto user = service.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final User.UserBuilder builder = User.withUsername(login);
        builder.password(user.getPasswordHash());
        @NotNull final List<RoleDto> userRole = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        userRole.forEach(role -> roles.add(role.toString()));
        builder.roles(roles.toArray(new String[]{}));
        @NotNull final UserDetails details = builder.build();
        @NotNull final User userSpring = (User) details;
        @NotNull final CustomUser customUser = new CustomUser(userSpring, user);
        return customUser;
    }

}
